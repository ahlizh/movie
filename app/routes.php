<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/', function() { return View::make('movie'); });
Route::get('/', 'BioskopController@index');
Route::get('/admin', 'BioskopController@login');
Route::post('/login', 'LoginController@userLogin');
Route::get('/logout', 'LoginController@userLogout');
Route::get('/movie/get_city', 'BioskopController@getCity');
Route::get('/movie/get_cinema', 'BioskopController@getCinema');
Route::get('/movie/get_movie', 'BioskopController@getMovie');
Route::get('/{city}', 'BioskopController@index');
Route::get('/{city}/{cinema}', 'BioskopController@index');
