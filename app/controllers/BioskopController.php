<?php

class BioskopController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index() {
		$cities = City::all();
		$bioskop = Bioskop::all();
		$movies = DB::table('bioskop_movie')
					->join('bioskop', 'bioskop_movie.bioskop', '=', 'bioskop.id')
					->join('movie', 'bioskop_movie.movie', '=', 'movie.id')
					->select('bioskop.id AS bioskop_id', 'movie.title', 'movie.id', 'movie.image', 'movie.description')
					->get();

		return View::make('movie')
					->with('cities', $cities)
					->with('bioskops', $bioskop)
					->with('movies', $movies);
	}

	public function getCity() {
		$cities = City::all();		
		return Response::json($cities);
	}

	public function getCinema() {
		$cinema = Bioskop::all();
		return Response::json($cinema);
	}

	public function getMovie() {
		$movie = DB::table('bioskop_movie')
					->join('bioskop', 'bioskop_movie.bioskop', '=', 'bioskop.id')
					->join('movie', 'bioskop_movie.movie', '=', 'movie.id')
					->select('bioskop.id AS bioskop_id', 'movie.title', 'movie.id', 'movie.image', 'movie.description')
					->get();
		return Response::json($movie);
	}

	public function login(){
		return View::make('admin');
	}
}
