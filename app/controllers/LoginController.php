<?php

class LoginController extends BaseController {


	public function userLogin(){

		$rules = array(
				'username'    => 'required',
				'password' => 'required|alphaNum|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('/admin')
								->withErrors($validator)
								->withInput(Input::except('password'));
		} else {
			$username = Input::get('username');
			$password = Input::get('password');

			$userdata = array (
					'username' => $username,
					'password' => $password
				);								
			
			if (Auth::attempt(array('email' => $username, 'password' => $password), true)) {
				echo 'Success';
			} else {
				return Redirect::to('/admin');				
			}

		}
	}

	public function userLogout() {
		Auth::logout();
		return Redirect::to('/admin');
	}
}
