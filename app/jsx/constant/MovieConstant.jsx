var KeyMirror = require('fbjs/lib/keyMirror');

module.exports = KeyMirror({
	// Load city data
	RECEIVE_CITY_DATA: null,
	RECEIVE_CINEMA_DATA: null,
	RECEIVE_MOVIE_DATA: null
});