var MovieDispatcher = require('../dispatcher/MovieDispatcher.jsx');
var EventEmitter = require('events').EventEmitter;
var MovieConstant = require('../constant/MovieConstant.jsx');
var _ = require('underscore');
var request = require('superagent');

var _city = [];
var _cinema = [];
var _movie = [];

/*function loadCityData(data) {
	_city = data;
}*/
function loadCityData() {
	var url = '/movie/get_city' 
		request
	    .get(url)
	    .end( function(err, response){
	    	_city = JSON.parse(response.text);	    	
	       MovieStore.emitChange();	     
    });
}

function loadCinemaData() {
	var url = '/movie/get_cinema'
		request
		.get(url)
		.end( function(err, response) {
			_cinema = JSON.parse(response.text);
			MovieStore.emitChange();
	});
}

function loadMovieData() {
	var url = '/movie/get_movie'
		request
		.get(url)
		.end( function(err, response){
			_movie = JSON.parse(response.text);			
			MovieStore.emitChange();
	});
}

var MovieStore = _.extend ({}, EventEmitter.prototype, {

	getCity: function() {
		return _city;
	},

	getCinema: function() {
		return _cinema;
	},

	getMovie: function() {
		return _movie;
	},

	emitChange: function() {
		this.emit('change');
	},

	addChangeListener: function(callback) {
		this.on('change', callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener('change', callback)
	}

});


MovieDispatcher.register(function(payload) {
	var action = payload.action;
	var text;

	switch(action.actionType) {
		case MovieConstant.RECEIVE_CITY_DATA:
			loadCityData();
			break;
		case MovieConstant.RECEIVE_CINEMA_DATA:
			loadCinemaData();
			break;
		case MovieConstant.RECEIVE_MOVIE_DATA:
			loadMovieData();
			break;
		default:
			return true;
	}

	MovieStore.emitChange();

	return true;
});

module.exports = MovieStore;