var MovieDispatcher = require('../dispatcher/MovieDispatcher.jsx');
var EventEmitter = require('events').EventEmitter;
var LoginConstant = require('../constant/LoginConstant.jsx');
var _ = require('underscore');
var request = require('superagent');

var _user = null;

function userLogin(data) {
	var url = '/user/login';
	request
	    .get(url)
	    .send(data)
	    .end( function(err, response){
	    	_user = JSON.parse(response.text);
	       	LoginStore.emitChange();
    });
}


var LoginStore = _.extend ({}, EventEmitter.prototype, {

	emitChange: function() {
		this.emit('change');
	},

	addChangeListener: function(callback) {
		this.on('change', callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener('change', callback)
	}

});


MovieDispatcher.register(function(payload) {
	var action = payload.action;
	var text;

	switch(action.actionType) {
		case LoginConstant.USER_LOGIN:
			loadCityData(action.data);
			break;
		default:
			return true;
	}

	LoginStore.emitChange();

	return true;
});

module.exports = LoginStore;