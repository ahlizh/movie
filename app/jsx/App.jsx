var React = require('react');
var ReactDOM = require('react-dom');

var superRouter = require('react-router');
var Router = superRouter.Router;
var Route = superRouter.Route;
var createBrowserHistory = require('history/lib/createBrowserHistory');

var Menu = require('./components/movie/Main.jsx');
var ContentMovie = require('./components/movie/ContentMovie.jsx');
var ListMovie = require('./components/movie/ListMovie.jsx');
var MainAdmin = require('./components/admin/Main.jsx');

var App = React.createClass({
	render : function(){
		return (
		<div>
			<div className="col-sm-2 sidenav">
				<Menu history={this.props.history}/>
			</div>
			{this.props.children}
		</div>
		)
	}
})

ReactDOM.render(
				<Router history={createBrowserHistory()}>
					<Route path="/" component={App}>
						<Route path="/:City" component={ContentMovie} >
							<Route path="/:City/:Cinema" component={ListMovie} />
						</Route>
					</Route>
					<Route name="main_admin" path="/admin" component={MainAdmin} />
				</Router>, 
				document.getElementById('react-container'));