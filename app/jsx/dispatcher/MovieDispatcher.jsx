var Dispatcher = require('flux').Dispatcher;

var MovieDispatcher = new Dispatcher();

MovieDispatcher.handleAction = function(action) {
	this.dispatch({
		source: 'VIEW_CINEMA',
		action: action
	})
}

module.exports = MovieDispatcher;