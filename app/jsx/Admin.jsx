var React = require('react');
var ReactDOM = require('react-dom');

var superRouter = require('react-router');
var Router = superRouter.Router;
var Route = superRouter.Route;
var createBrowserHistory = require('history/lib/createBrowserHistory');

var Main = require('./components/admin/Main.jsx');

var App = React.createClass({
	render : function(){
		return (
		<div>
			<div className="col-sm-2 sidenav">
				<Main />
			</div>
			{this.props.children}
		</div>
		)
	}
})

ReactDOM.render(
				<Router history={createBrowserHistory()}>
					<Route path="/admin" component={App}>
					</Route>
				</Router>,
				document.getElementById('react-container'));