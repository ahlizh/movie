var React = require('react');
var ReactDOM = require('react-dom');

var LoginAction = require('../../actions/LoginAction.jsx');
var LoginStore = require('../../stores/LoginStore.jsx');

var superRouter = require('react-router');

var Router = superRouter.Router;
var Route = superRouter.Route;
var Link = superRouter.Link;
var RouteHandler = superRouter.RouteHandler;
var DefaultRoute = superRouter.DefaultRoute;
var Navigation = superRouter.Navigation;

var createBrowserHistory = require('history/lib/createBrowserHistory');

var Main = React.createClass({

	goToContent: function(url) {
		this.props.history.replaceState(null, url);
	},

	logout: function() {
		this.transitionTo('/logout');
	},



	render: function() {		
		return (
			<div>	
				<ul>
					<li>Hi {USER_LOGGED_IN}, <a onClick={this.goToContent.bind(this, '/logout')}>Logout</a></li>
					<li>Add City</li>
					<li>Add Cinema</li>
				</ul>
			</div>
		)
	}
});

module.exports = Main;