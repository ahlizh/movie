var React = require('react');
var ReactDOM = require('react-dom');
var Modal = require('react-modal');

var superRouter = require('react-router');

var Router = superRouter.Router;
var Route = superRouter.Route;
var Link = superRouter.Link;
var RouteHandler = superRouter.RouteHandler;
var DefaultRoute = superRouter.DefaultRoute;

var createBrowserHistory = require('history/lib/createBrowserHistory');
var DetailMovieModal = require('./DetailMovieModal.jsx');
var ListMovie = require('./ListMovie.jsx');
var MovieAction = require('../../actions/MovieAction.jsx');
var MovieStore = require('../../stores/MovieStore.jsx');

var ContentMovie = React.createClass({
	getInitialState: function() {
		return {
			cities: MovieStore.getCity(),
			selectMovie: [],
			city: '',
			cityId: 0,
			cinema: []
		};
	},

	componentDidMount: function() {
		MovieStore.addChangeListener(this._onChange);
		MovieAction.receiveCityData();
		MovieAction.receiveCinemaData();
	},

	componentWillMount: function() {
		this.setState({cityId: this.getCityId(this.props.params.City)});
		MovieStore.removeChangeListener(this._onChange);				
	},

	_onChange: function() {
		this.setState({			
			cities: MovieStore.getCity(),
			cinema: MovieStore.getCinema()
		});
	},

	componentWillReceiveProps: function(nextProps){
			var bioskopid = 0;
			if (nextProps.params.Cinema) {
				bioskopid = this.getBioskopId(nextProps.params.Cinema);
			}

			this.setState({
				bioskopId: bioskopid,
				cityId: this.getCityId(nextProps.params.City),
				cinema: MovieStore.getCinema()					
			});
	},

	getListBioskop: function(id) {
		return (
			<div>
				<ListMovie bioskopId={id} />
			</div>
		)
	},

	goToCinema: function(url) {
		this.props.history.replaceState(null, url);
	},

	getCityId: function(cityName) {
		var parent = this;
		var cityid = 0;
		if (cityName) {
			this.state.cities.map(function(city) {
				if (city.name == cityName) {
					cityid = city.id;
				}
			})
		}
		return cityid;
	},

	getBioskopId: function(cinemaName) {
		var bioskopid = 0;
		if (cinemaName) {
			this.state.cinema.map(function(cinema) {
				if (cinema.key_url == cinemaName) {
					bioskopid = cinema.id;
				}
			})
		}
		return bioskopid;
	},


	render: function() {
		var parent = this;
		var bioskopId = this.getBioskopId(this.props.params.Cinema);
		return (
			<div className="col-sm-10 text-left">
				<ul className="nav nav-pills" role="tablist">
					{this.state.cinema.map(function(dataBioskop) {
						if (dataBioskop.city == this.state.cityId) {
							var url = '/' + this.props.params.City + '/' + dataBioskop.key_url;
							return (
								<li key={dataBioskop.id} className="active"><a onClick={this.goToCinema.bind(this, url)}>{dataBioskop.name}</a></li>
							)
						}
					}.bind(this))}

					{this.getListBioskop(bioskopId)}
				</ul>
			</div>
		)
	}
})

module.exports = ContentMovie;