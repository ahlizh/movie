var React = require('react');
var ReactDOM = require('react-dom');
var superRouter = require('react-router');
var Bootstrap = require('react-bootstrap');

var Route = superRouter.Route;
var Router = superRouter.Router;

var DetailMovieModal = require('./DetailMovieModal.jsx');
var MovieAction = require('../../actions/MovieAction.jsx');
var MovieStore = require('../../stores/MovieStore.jsx');

var ListMovie = React.createClass({
	getInitialState: function() {
		return {
			selectMovie: 0,
			modalIsOpen: false,
			movies: []
		}
	},

	componentDidMount: function() {
		MovieStore.addChangeListener(this._onChange);
		MovieAction.receiveMovieData();
	},

	componentWillMount: function() {
		MovieStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		this.setState({
			movies: MovieStore.getMovie()
		});
	},

	openMovieModal: function(id){
		this.setState({
			modalIsOpen: true,
			selectMovie: id
		});
	},

	closeMovieModal: function() {
		this.setState({
			selectMovie: 0
		});
	},

	render: function() {
		var bioskopId = this.props.bioskopId;
		var movie = MOVIES_ENCODE;
		var parent = this;
		var modalOpen = false;
		return (
			<div>
			{this.state.movies.map(function(film) {
				var img = "../img/" + film.image;
				var movieListDetail = null;
				if (film.bioskop_id == bioskopId) {
					return(
						<div key={film.title}>
							<div>
								<br/><br/><br/>
								<div onClick={parent.openMovieModal.bind(this, film.id)}>{film.title}</div>
								<img src={img} />
								<div>{film.description}</div>
							</div>
							<DetailMovieModal closeMovieModal={this.closeMovieModal} movie={film} isOpen={film.id == this.state.selectMovie} />
						</div>
					)
				}
			}.bind(this))}
			</div>
		)
	}
})

module.exports = ListMovie;
