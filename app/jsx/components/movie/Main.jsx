var React = require('react');
var ReactDOM = require('react-dom');

var MovieAction = require('../../actions/MovieAction.jsx');
var MovieStore = require('../../stores/MovieStore.jsx');

var superRouter = require('react-router');

var Router = superRouter.Router;
var Route = superRouter.Route;
var Link = superRouter.Link;
var RouteHandler = superRouter.RouteHandler;
var DefaultRoute = superRouter.DefaultRoute;
var Navigation = superRouter.Navigation;

var createBrowserHistory = require('history/lib/createBrowserHistory');


var ContentMovie = require('./ContentMovie.jsx');
var ListMovie = require('./ListMovie.jsx');

var Main = React.createClass({

	getInitialState: function() {
		return {
			city: []
		}			
	},

	componentDidMount: function() {		
		MovieStore.addChangeListener(this._onChange);
		MovieAction.receiveCityData();
	},

	componentWillMount: function() {
		MovieStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		this.setState({
			city: MovieStore.getCity()
		});
	},

	goToContent: function(url) {
		this.props.history.replaceState(null, url);
	},

	openAddMovie: function() {
		this.setState({
			addMovieIsOpen: true
		});
	},

	closeAddMovie: function() {
		this.setState({
			addMovieIsOpen: false
		});
	},

	render: function() {
		var parent = this;
		return (
			<div>					
				<ul>
					{this.state.city.map(function(ct){
						var url = "/" + ct.name;							
						return (
							<li key={ct.id}><a onClick={parent.goToContent.bind(this, url)} >{ct.name}</a></li>
						)
					}.bind(this))}
				</ul>
			</div>
		)
	}
});

module.exports = Main;