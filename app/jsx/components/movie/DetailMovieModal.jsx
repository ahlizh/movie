var React = require('react');
var ReactDOM = require('react-dom');
var superRouter = require('react-router');
var Bootstrap = require('react-bootstrap');
var Modal = require('react-modal');

var Route = superRouter.Route;
var Router = superRouter.Router;

var DetailMovieModal = require('./DetailMovieModal.jsx');

var DetailMovieModal = React.createClass({	
	render: function() {
		var img = "../img/" + this.props.movie.image;
		return (
			<Modal isOpen={this.props.isOpen} onRequestClose={this.closeModal}>
				<div>
					<button onClick={this.props.closeMovieModal}>close</button>
					<div>{this.props.movie.title}</div>
					<img src={img} />
					<div>{this.props.movie.description}</div>
				</div>
			</Modal>
		)
	}
})

module.exports = DetailMovieModal;