var React = require('react');
var ReactDOM = require('react-dom');
var superRouter = require('react-router');
var Bootstrap = require('react-boostrap');
var Modal = require('react-modal');

var Route = superRouter.Route;
var Router = superRouter.Router;

var AddCinemaModal = React.createClass({
	render:: function() {
		return (
			<Modal isOpen={this.props.isOpen} >
				<div className="app">
					<button onClick={this.props.closeModal}>close</button>
					<h1>Add Cinema</h1>
				</div>
			</Modal>
		)
	}
})

module.exports = AddCinemaModal;