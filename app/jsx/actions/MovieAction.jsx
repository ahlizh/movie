var MovieDispatcher = require('../dispatcher/MovieDispatcher.jsx');
var MovieConstant = require('../constant/MovieConstant.jsx');

var MovieAction = {

	// Receive city data
	/*receiveCityData: function(data){
		MovieDispatcher.handleAction({
			actionType: MovieConstant.RECEIVE_CITY_DATA,
			data: data	
		})
	},*/
	receiveCityData: function(){
		MovieDispatcher.handleAction({
			actionType: MovieConstant.RECEIVE_CITY_DATA			
		})
	},

	receiveCinemaData: function(){
		MovieDispatcher.handleAction({
			actionType: MovieConstant.RECEIVE_CINEMA_DATA			
		})
	},

	receiveMovieData: function(){
		MovieDispatcher.handleAction({
			actionType: MovieConstant.RECEIVE_MOVIE_DATA			
		})
	}
};

module.exports = MovieAction;