var MovieDispatcher = require('../dispatcher/MovieDispatcher.jsx');
var LoginConstant = require('../constant/LoginConstant.jsx');

var LoginAction = {

	userLoggedIn: function(data) {
		MovieDispatcher.handleAction({
			actionType: LoginConstant.USER_LOGIN,
			data: data
		})
	}
};

module.exports = LoginAction;